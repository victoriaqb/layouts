//
//  main.m
//  SpamLayout
//
//  Created by Victoria Kashlina on 19/08/2018.
//  Copyright © 2018 None. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
