//
//  AppDelegate.h
//  SpamLayout
//
//  Created by Victoria Kashlina on 19/08/2018.
//  Copyright © 2018 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

